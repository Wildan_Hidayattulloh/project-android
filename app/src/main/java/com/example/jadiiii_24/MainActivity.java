package com.example.jadiiii_24;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.MediaController;

public class MainActivity extends AppCompatActivity {


    ListView list;
    String[] namabuah = {
            "Alpukat",
            "Aple",
            "Ceri",
            "Durian",
            "Jambu Air",
            "Manggis",
            "Strawberry",
            "musik"
    };

    int[] gambarbuah = {
            //ctrl+D utk mengcopy satu baris
            R.drawable.alpukat,
            R.drawable.apel,
            R.drawable.ceri,
            R.drawable.durian,
            R.drawable.jambuair,
            R.drawable.manggis,
            R.drawable.strawberry,
            R.drawable.tebakbuah,
    };

    int[] suarabuah = {
            //ctrl+R utk me Replace
            R.raw.alpukat,
            R.raw.apel,
            R.raw.ceri,
            R.raw.durian,
            R.raw.jambuair,
            R.raw.manggis,
            R.raw.strawberry,
            R.raw.faded
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = findViewById(R.id.list_view);

//        ArrayAdapter adapter = new ArrayAdapter(MainActivity.this,android.R.layout.simple_list_item_1,namabuah);
        CustomListAdapter adapter = new CustomListAdapter(MainActivity.this, namabuah, gambarbuah);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                MediaPlayer.create(MainActivity.this, suarabuah[position]).start();
            }
        });
    }
}