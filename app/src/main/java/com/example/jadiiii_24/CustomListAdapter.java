package com.example.jadiiii_24;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter {
    private Context context;
    private String[] nabuh;
    private int[] gambuh;

    //utk membangun contractor = klik kanan -> generat -> contractor

    public CustomListAdapter(Context context1, String[] nabuh, int[] gambuh) {
        super(context1, R.layout.item_buah, nabuh);
        this.context = context1;
        this.nabuh = nabuh;
        this.gambuh = gambuh;
    }

    //getview

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //atur layout itemnya

        View view = LayoutInflater.from(context).inflate(R.layout.item_buah, parent, false);

        //findViewById setipa komponen yg di dalam item

        TextView namefruit = view.findViewById(R.id.buah_alpokat);
        ImageView imagefruit = view.findViewById(R.id.alpukat);

        //set data

        namefruit.setText(nabuh[position]);
        imagefruit.setImageResource(gambuh[position]);

        return view;
    }
}
